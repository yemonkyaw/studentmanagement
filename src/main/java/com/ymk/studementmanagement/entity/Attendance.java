package com.ymk.studementmanagement.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sm_attendance")
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,  generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private int aid;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "student_id")
    @NotNull(message = "Student Required")
    private Student studentId;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "class_id")
    @NotNull(message = "Class Required")
    private Classes classId;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "teacher_id")
    @NotNull(message = "Teacher Required")
    private Teacher teacherId;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "subject_id")
    @NotNull(message = "Subject Required")
    private Subject subjectId;

    @Column(columnDefinition="TEXT")
    private String absents;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date date;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date entry;
}
