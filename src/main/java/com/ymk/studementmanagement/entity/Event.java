package com.ymk.studementmanagement.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sm_events")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,  generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private int id;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @NotNull(message = "StartDate Required")
    private Date start;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @NotNull(message = "EndDate Required")
    private Date end;

    private String type;

    @Column(columnDefinition = "TEXT")
    private String title;

    @Column(columnDefinition = "LONGTEXT")
    private String description;

    private String color;

}
