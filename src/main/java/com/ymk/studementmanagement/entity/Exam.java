package com.ymk.studementmanagement.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sm_exam")
public class Exam {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,  generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private int eid;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "classid")
    private Classes classId;

    @Column(name = "e_name")
    private String eName;

    @Column(name = "e_s_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date esDate;

    @Column(name = "e_e_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date eeDate;

    @Column(name = "entry_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date entryDate;
}
