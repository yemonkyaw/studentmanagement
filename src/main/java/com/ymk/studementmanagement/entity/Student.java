package com.ymk.studementmanagement.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.TemporalType.DATE;

@Getter
@Setter
@Entity
@Table(name = "sm_student")
public class Student {
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
  @GenericGenerator(name = "native", strategy = "native")
  private Integer sid;
  @Column(name="s_rollno")
  @NotBlank(message = "Roll Number Required")
  private String sRollNo;
  @Column(name="s_name")
  @NotBlank(message = "Name Required")
  private String sName;
  @Column(name="s_dob")
  @DateTimeFormat(pattern="yyyy-MM-dd")
  private Date sDob;
  @Column(name = "s_gender")
  @NotBlank(message = "Gender Required")
  private String sGender;
  @Column(name = "s_paddress")
  @NotBlank(message = "Parent Address Required")
  private String sPAddress;
  @Column(name = "s_phone")
  private String sPhone;
  @Column(name = "s_bloodgrp")
  @NotBlank(message = "Blood Group Required")
  private String sBloodGrp;
  @Column(name = "s_doj")
  @DateTimeFormat (pattern="yyyy-MM-dd")
  private Date sDoj;
  @Column(name = "class_id")
  private String classId;
  @Column(name = "class_date")
  private String classDate;
  @Column(name = "p_name")
  @NotBlank(message = "Parent Name Required")
  private String pName;
  @Column(name = "p_gender")
  @NotBlank(message = "Parent Gender Required")
  private String pGender;
  @Column(name = "p_edu")
  private String pEdu;
  @Column(name = "p_phone")
  @NotBlank(message = "Parent Phone Required")
  private String pPhone;
  @Column(name = "p_profession")
  private String pProfession;
  @Column(name = "p_bloodgrp")
  private String pBloodGrp;
}
