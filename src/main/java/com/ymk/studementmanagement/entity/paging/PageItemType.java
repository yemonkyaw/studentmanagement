package com.ymk.studementmanagement.entity.paging;

public enum PageItemType {

    DOTS,
    PAGE

}
