package com.ymk.studementmanagement.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name = "sm_subject")
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private int id;

    @Column(name = "sub_code")
    @NotBlank(message = "Subject Code Required")
    private String subCode;

    @JoinColumn(name = "class_id")
    @NotNull(message = "Class Required")
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Classes classId;

    @Column(name = "sub_name")
    @NotBlank(message = "Subject Name Required")
    private String subName;

    @JoinColumn(name = "sub_teach_id")
    @NotNull(message = "Teacher Required")
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Teacher teacher;

    @Column(name = "book_name")
    private String bookName;

    @Column(name = "sub_desc")
    private String description;

    @Column(name = "max_mark")
    private int maxMark;

    @Column(name = "pass_mark")
    private int passMark;

}
