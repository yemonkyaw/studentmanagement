package com.ymk.studementmanagement.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sm_teacher")
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,  generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private int tid;

    @NotBlank(message = "Name required")
    private String name;

    @NotBlank(message = "Address Required")
    private String address;

//    @NotBlank(message = "Date of birth required")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date dob;

//    @NotBlank(message = "Date of join required")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date doj;

    @NotBlank(message = "Phone Number required")
    private String phone;

    @NotBlank(message = "Qualification required")
    private String qualification;

    private String gender;

    @Column(name = "bloodgrp")
    private String bloodGrp;

    private String position;

    @Column(name = "whours")
    private String wHours;
}
