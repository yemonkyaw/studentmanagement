package com.ymk.studementmanagement.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "sm_mark")
public class Mark {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,  generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private int mid;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "student_id")
    private Student studentId;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "subject_id")
    private Subject subjectId;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "exam_id")
    private Exam examId;

    private String mark;

    @Column(columnDefinition = "TEXT")
    private String remarks;

    private String attendance;

}
