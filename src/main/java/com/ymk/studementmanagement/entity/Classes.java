package com.ymk.studementmanagement.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sm_class")
public class Classes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,  generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private int cid;

    @Column(name = "c_numb")
    @NotBlank(message = "Number Required")
    private String cNumber;

    @Column(name = "c_name")
    @NotBlank(message = "Name Required")
    private String cName;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "teacher_id")
    @NotNull(message = "Teacher Required")
    private Teacher teacherId;

    @Column(name = "c_capacity")
    @NotNull(message = "Capacity Required")
    private int capacity;

    @Column(name = "c_loc")
    private String location;

    @Column(name = "c_sdate")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date startDate;

    @Column(name = "c_edate")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date endDate;

}
