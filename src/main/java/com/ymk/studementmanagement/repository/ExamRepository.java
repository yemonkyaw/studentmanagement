package com.ymk.studementmanagement.repository;

import com.ymk.studementmanagement.entity.Exam;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamRepository extends JpaRepository<Exam,Integer> {
}
