package com.ymk.studementmanagement.repository;

import com.ymk.studementmanagement.entity.Mark;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarkRepository extends JpaRepository<Mark,Integer> {
}
