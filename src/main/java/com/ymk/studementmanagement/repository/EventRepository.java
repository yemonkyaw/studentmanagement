package com.ymk.studementmanagement.repository;

import com.ymk.studementmanagement.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EventRepository extends JpaRepository<Event, Integer> {
}
