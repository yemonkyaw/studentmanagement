package com.ymk.studementmanagement.repository;

import com.ymk.studementmanagement.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Integer> {
}
