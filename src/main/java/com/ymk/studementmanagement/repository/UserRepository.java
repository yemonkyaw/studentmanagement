package com.ymk.studementmanagement.repository;

import com.ymk.studementmanagement.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users, Integer> {
    Users findFirstByName(String name);
}
