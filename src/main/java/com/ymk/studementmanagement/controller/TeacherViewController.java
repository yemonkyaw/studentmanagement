package com.ymk.studementmanagement.controller;

import com.ymk.studementmanagement.entity.Teacher;
import com.ymk.studementmanagement.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class TeacherViewController {
    @Autowired
    private TeacherService teacherService;

    @InitBinder
    protected void init(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    @GetMapping("/newteacher")
    public String newTeacher(Teacher teacher) {
        return "teacher/newteacher";
    }

    @RequestMapping(value = "/teachers", method = RequestMethod.GET)
    public String getTeacher(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                              @RequestParam(value = "size", required = false, defaultValue = "10") int size, Model model){
        model.addAttribute("teachers", teacherService.getTeacherPaginaion(pageNumber, size));
        return "teacher/teachers";
    }

    @RequestMapping(value = "/add_teacher", method = RequestMethod.POST)
    public String addTeacher(@Valid Teacher teacher, BindingResult result, Model model){
        if (result.hasErrors()) {
            return "teacher/newteacher";
        }

        teacherService.createUpdateTeacher(teacher);
        return "redirect:/teachers";
    }

    @GetMapping("/editteacher/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        Teacher teacher = teacherService.findById(id);
        model.addAttribute("teacher", teacher);

        return "teacher/editteacher";
    }

    @PostMapping("/updateteacher/{id}")
    public String updateTeacher(@PathVariable("id") int id, @Valid Teacher teacher, BindingResult result, Model model) {
        teacher.setTid(id);
        if (result.hasErrors()) {
            return "teacher/editteacher";
        }

        teacherService.createUpdateTeacher(teacher);

        return "redirect:/teachers";
    }

    @GetMapping("/deleteteacher/{id}")
    public String deleteTeacher(@PathVariable("id") int id, Model model) {
        Teacher teacher = teacherService.findById(id);
        teacherService.deleteTeacher(teacher);

        return "redirect:/teachers";
    }
}
