package com.ymk.studementmanagement.controller;

import com.ymk.studementmanagement.entity.Attendance;
import com.ymk.studementmanagement.entity.Event;
import com.ymk.studementmanagement.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class EventViewController {

    @Autowired
    private EventService eventService;

    @GetMapping("/events")
    public String viewAllEvent(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                                    @RequestParam(value = "size", required = false, defaultValue = "10") int size, Model model){
        model.addAttribute("events", eventService.getAllEventsWithPaged(pageNumber, size));
        return "event/event";
    }

    @GetMapping("/event/new")
    public String newEvent(Event event, Model model){
        return "event/newevent";
    }

    @PostMapping("/event/add")
    public String addEvent(@Valid Event event, BindingResult result, Model model){
        if(result.hasErrors()){
            return "event/newevent";
        }
        eventService.saveUpdate(event);
        return "redirect:/events";
    }

    @GetMapping("/event/edit/{id}")
    public String editEvent(@PathVariable("id") int id, Model model){
        model.addAttribute("event",eventService.findById(id));
        return "event/editevent";
    }

    @PostMapping("/event/update/{id}")
    public String updateEvent(@PathVariable("id") int id, @Valid Event event, BindingResult result, Model model){
        event.setId(id);
        if(result.hasErrors()){
            return "event/editevent";
        }
        eventService.saveUpdate(event);
        return "redirect:/events";
    }

    @GetMapping("/event/delete/{id}")
    public String deleteEvent(@PathVariable("id") int id, Model model){
        Event event = eventService.findById(id);
        eventService.delete(event);
        return "redirect:/events";
    }

}
