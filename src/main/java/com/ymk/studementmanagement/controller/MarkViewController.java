package com.ymk.studementmanagement.controller;

import com.ymk.studementmanagement.entity.Mark;
import com.ymk.studementmanagement.service.ExamService;
import com.ymk.studementmanagement.service.MarkService;
import com.ymk.studementmanagement.service.StudentService;
import com.ymk.studementmanagement.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class MarkViewController {

    @Autowired
    private MarkService markService;

    @Autowired
    private ExamService examService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private SubjectService subjectService;

    @GetMapping("/mark")
    public String viewAllMark(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                                    @RequestParam(value = "size", required = false, defaultValue = "10") int size, Model model){
        model.addAttribute("marks", markService.getAllMarksWithPaged(pageNumber, size));
        return "mark/mark";
    }

    @GetMapping("/mark/new")
    public String newMark(Mark mark, Model model){
        model.addAttribute("students",studentService.getAllStudents());
        model.addAttribute("subjects",subjectService.getAllSubjects());
        model.addAttribute("exams",examService.getAllExams());
        return "mark/newmark";
    }

    @PostMapping("/mark/add")
    public String addMark(@Valid Mark mark, BindingResult result, Model model){
        if(result.hasErrors()){
            model.addAttribute("students",studentService.getAllStudents());
            model.addAttribute("subjects",subjectService.getAllSubjects());
            model.addAttribute("exams",examService.getAllExams());
            return "mark/newmark";
        }
        markService.saveUpdate(mark);
        return "redirect:/mark";
    }

    @GetMapping("/mark/edit/{id}")
    public String editMark(@PathVariable("id") int id, Model model){
        model.addAttribute("students",studentService.getAllStudents());
        model.addAttribute("subjects",subjectService.getAllSubjects());
        model.addAttribute("exams",examService.getAllExams());
        model.addAttribute("mark",markService.findById(id));
        return "mark/editmark";
    }

    @PostMapping("/mark/update/{id}")
    public String updateMark(@PathVariable("id") int id, @Valid Mark mark, BindingResult result, Model model){
        mark.setMid(id);
        if(result.hasErrors()){
            model.addAttribute("students",studentService.getAllStudents());
            model.addAttribute("subjects",subjectService.getAllSubjects());
            model.addAttribute("exams",examService.getAllExams());
            return "mark/editmark";
        }

        markService.saveUpdate(mark);
        return "redirect:/mark";
    }

    @GetMapping("/mark/delete/{id}")
    public String deleteMark(@PathVariable("id") int id, Model model){
        Mark mark = markService.findById(id);
        markService.delete(mark);
        return "redirect:/mark";
    }

}
