package com.ymk.studementmanagement.controller;

import com.alibaba.fastjson.JSONObject;
import com.ymk.studementmanagement.entity.Classes;
import com.ymk.studementmanagement.entity.Student;
import com.ymk.studementmanagement.service.ClassService;
import com.ymk.studementmanagement.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class ClassViewController {

    @Autowired
    private ClassService classService;

    @Autowired
    private TeacherService teacherService;

    @GetMapping("/classes")
    public String viewAllClass(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                               @RequestParam(value = "size", required = false, defaultValue = "10") int size, Model model){
        model.addAttribute("classes", classService.getClassesWithPaged(pageNumber, size));
        return "classes/classes";
    }

    @GetMapping("/classes/new")
    public String newClass(Classes classes, Model model){
        model.addAttribute("teachers", teacherService.getAllTeacher());
        return "classes/newclasses";
    }

    @PostMapping("/classes/add")
    public String addClass(@Valid Classes classes, BindingResult result, Model model){
        if(result.hasErrors()){
            model.addAttribute("teachers", teacherService.getAllTeacher());
            return "classes/newclasses";
        }
        classService.saveUpdate(classes);
        return "redirect:/classes";
    }

    @GetMapping("/classes/edit/{id}")
    public String editClass(@PathVariable("id") int id, Model model){
        model.addAttribute("classes",classService.findById(id));
        model.addAttribute("teachers", teacherService.getAllTeacher());
        return "classes/editclasses";
    }

    @PostMapping("/classes/update/{id}")
    public String updateClass(@PathVariable("id") int id, @Valid Classes classes, BindingResult result, Model model){
        classes.setCid(id);
        if(result.hasErrors()){
            model.addAttribute("teachers", teacherService.getAllTeacher());
            return "classes/editclasses";
        }

        classService.saveUpdate(classes);
        return "redirect:/classes";
    }

    @GetMapping("/classes/delete/{id}")
    public String deleteClass(@PathVariable("id") int id, Model model){
        Classes classes = classService.findById(id);
        classService.delete(classes);
        return "redirect:/classes";
    }

}
