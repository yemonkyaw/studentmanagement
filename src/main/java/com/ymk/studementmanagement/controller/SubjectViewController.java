package com.ymk.studementmanagement.controller;

import com.ymk.studementmanagement.entity.Subject;
import com.ymk.studementmanagement.service.ClassService;
import com.ymk.studementmanagement.service.SubjectService;
import com.ymk.studementmanagement.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class SubjectViewController {

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private ClassService classService;

    @GetMapping("/subjects")
    public String showSubjects(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                               @RequestParam(value = "size", required = false, defaultValue = "10") int size, Model model){
        model.addAttribute("subjects", subjectService.getSubjectsWithPaged(pageNumber, size));
        return "subject/subject";
    }

    @GetMapping("/subjects/new")
    public String newSubject(Subject subject, Model model){
        model.addAttribute("teachers", teacherService.getAllTeacher());
        model.addAttribute("classes", classService.getAllClasses());
        return "subject/newsubject";
    }

    @PostMapping("/subjects/add")
    public String addSubject(@Valid Subject subject, BindingResult result, Model model){
        if(result.hasErrors()){
            model.addAttribute("teachers", teacherService.getAllTeacher());
            model.addAttribute("classes", classService.getAllClasses());
            return "subject/newsubject";
        }
        subjectService.saveUpdate(subject);
        return "redirect:/subjects";
    }

    @GetMapping("/subjects/edit/{id}")
    public String editSubject(@PathVariable("id") int id, Model model){
        model.addAttribute("subject",subjectService.findById(id));
        model.addAttribute("teachers", teacherService.getAllTeacher());
        model.addAttribute("classes", classService.getAllClasses());
        return "subject/editsubject";
    }

    @PostMapping("/subjects/update/{id}")
    public String updateSubject(@PathVariable("id") int id, @Valid Subject subject, BindingResult result, Model model){
        subject.setId(id);
        if(result.hasErrors()){
            model.addAttribute("teachers", teacherService.getAllTeacher());
            model.addAttribute("classes", classService.getAllClasses());
            return "subject/editsubject";
        }

        subjectService.saveUpdate(subject);
        return "redirect:/subjects";
    }

    @GetMapping("/subjects/delete/{id}")
    public String deleteSubject(@PathVariable("id") int id, Model model){
        Subject subject = subjectService.findById(id);
        subjectService.delete(subject);
        return "redirect:/subjects";
    }
}
