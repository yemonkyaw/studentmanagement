package com.ymk.studementmanagement.controller;

import com.ymk.studementmanagement.entity.Attendance;
import com.ymk.studementmanagement.entity.Exam;
import com.ymk.studementmanagement.service.ClassService;
import com.ymk.studementmanagement.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class ExamViewController {

    @Autowired
    private ExamService examService;

    @Autowired
    private ClassService classService;

    @GetMapping("/exam")
    public String viewAllExam(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                                    @RequestParam(value = "size", required = false, defaultValue = "10") int size, Model model){
        model.addAttribute("exams", examService.getAllExamsWithPaged(pageNumber, size));
        return "exam/exam";
    }

    @GetMapping("/exam/new")
    public String newExam(Exam exam, Model model){
        model.addAttribute("classes", classService.getAllClasses());
        return "exam/newexam";
    }

    @PostMapping("/exam/add")
    public String addExam(@Valid Exam exam, BindingResult result, Model model){
        if(result.hasErrors()){
            model.addAttribute("classes", classService.getAllClasses());
            return "exam/newexam";
        }
        examService.saveUpdate(exam);
        return "redirect:/exam";
    }

    @GetMapping("/exam/edit/{id}")
    public String editExam(@PathVariable("id") int id, Model model){
        model.addAttribute("classes", classService.getAllClasses());
        model.addAttribute("exam",examService.findById(id));
        return "exam/editexam";
    }

    @PostMapping("/exam/update/{id}")
    public String updateExam(@PathVariable("id") int id, @Valid Exam exam, BindingResult result, Model model){
        exam.setEid(id);
        if(result.hasErrors()){
            model.addAttribute("classes", classService.getAllClasses());
            return "exam/editexam";
        }

        examService.saveUpdate(exam);
        return "redirect:/exam";
    }

    @GetMapping("/exam/delete/{id}")
    public String deleteExam(@PathVariable("id") int id, Model model){
        Exam exam = examService.findById(id);
        examService.delete(exam);
        return "redirect:/exam";
    }


}
