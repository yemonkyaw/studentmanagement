package com.ymk.studementmanagement.controller;

import com.ymk.studementmanagement.entity.Attendance;
import com.ymk.studementmanagement.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class AttendanceViewController {

    @Autowired
    private ClassService classService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private AttendanceService attendanceService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private TeacherService teacherService;

    @GetMapping("/attendance")
    public String viewAllAttendance(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                               @RequestParam(value = "size", required = false, defaultValue = "10") int size, Model model){
        model.addAttribute("attendances", attendanceService.getAllAttendancesWithPaged(pageNumber, size));
        return "attendance/attendance";
    }

    @GetMapping("/attendance/new")
    public String newAttendance(Attendance attendance, Model model){
        model.addAttribute("classes", classService.getAllClasses());
        model.addAttribute("students",studentService.getAllStudents());
        model.addAttribute("teachers",teacherService.getAllTeacher());
        model.addAttribute("subjects",subjectService.getAllSubjects());
        return "attendance/newattendance";
    }

    @PostMapping("/attendance/add")
    public String addAttendance(@Valid Attendance attendance, BindingResult result, Model model){
        if(result.hasErrors()){
            model.addAttribute("classes", classService.getAllClasses());
            model.addAttribute("students",studentService.getAllStudents());
            model.addAttribute("teachers",teacherService.getAllTeacher());
            model.addAttribute("subjects",subjectService.getAllSubjects());
            return "attendance/newattendance";
        }
        attendanceService.saveUpdate(attendance);
        return "redirect:/attendance";
    }

    @GetMapping("/attendance/edit/{id}")
    public String editAttendance(@PathVariable("id") int id, Model model){
        model.addAttribute("classes", classService.getAllClasses());
        model.addAttribute("students",studentService.getAllStudents());
        model.addAttribute("teachers",teacherService.getAllTeacher());
        model.addAttribute("subjects",subjectService.getAllSubjects());
        model.addAttribute("attendance",attendanceService.findById(id));
        return "attendance/editattendance";
    }

    @PostMapping("/attendance/update/{id}")
    public String updateAttendance(@PathVariable("id") int id, @Valid Attendance attendance, BindingResult result, Model model){
        attendance.setAid(id);
        if(result.hasErrors()){
            model.addAttribute("classes", classService.getAllClasses());
            model.addAttribute("students",studentService.getAllStudents());
            model.addAttribute("teachers",teacherService.getAllTeacher());
            model.addAttribute("subjects",subjectService.getAllSubjects());
            return "attendance/editattendance";
        }

        attendanceService.saveUpdate(attendance);
        return "redirect:/attendance";
    }

    @GetMapping("/attendance/delete/{id}")
    public String deleteAttendance(@PathVariable("id") int id, Model model){
        Attendance attendance = attendanceService.findById(id);
        attendanceService.delete(attendance);
        return "redirect:/attendance";
    }

}
