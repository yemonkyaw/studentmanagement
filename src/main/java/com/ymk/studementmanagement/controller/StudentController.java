package com.ymk.studementmanagement.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ymk.studementmanagement.entity.Student;
import com.ymk.studementmanagement.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ymk.studementmanagement.model.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping("/addstudent")
    public String addStudent(@RequestBody JSONObject jsonparan) throws ParseException {
        Student newstudent = JSON.parseObject(jsonparan.toJSONString(), Student.class);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        newstudent.setSDob(sdf.parse(sdf.format(newstudent.getSDob())));
        newstudent.setSDoj(sdf.parse(sdf.format(newstudent.getSDoj())));

        studentService.addStudent(newstudent);

        Message sm = new Message();
        sm.setCode(1);
        sm.setMessage("已导入一条数据");

        String str = JSON.toJSONString(sm);
        return str;
    }

}
