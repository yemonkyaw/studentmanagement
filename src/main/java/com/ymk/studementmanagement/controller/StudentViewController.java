package com.ymk.studementmanagement.controller;

import com.ymk.studementmanagement.entity.Student;
import com.ymk.studementmanagement.repository.StudentRepository;
import com.ymk.studementmanagement.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class StudentViewController{

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentRepository studentRepository;

    @InitBinder
    protected void init(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    @GetMapping("/newstudent")
    public String newStudent(Student student) {
        return "student/newstudent";
    }

    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public String getStudents(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                              @RequestParam(value = "size", required = false, defaultValue = "10") int size, Model model){
        model.addAttribute("students", studentService.getAllStudentsWithPaged(pageNumber, size));
        return "student/students";
    }

    @RequestMapping(value = "/add_student", method = RequestMethod.POST)
    public String addStudent(@Valid Student student, BindingResult result, Model model){
        if (result.hasErrors()) {
            return "student/newstudent";
        }

        studentService.addStudent(student);
        return "redirect:/students";
    }

    @GetMapping("/editstudent/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        Student student = studentRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
        model.addAttribute("student", student);

        return "student/editstudent";
    }

    @PostMapping("/updatestudent/{id}")
    public String updateStudent(@PathVariable("id") int id, @Valid Student student, BindingResult result, Model model) {
        student.setSid(id);
        if (result.hasErrors()) {
            return "student/editstudent";
        }

        studentService.addStudent(student);

        return "redirect:/students";
    }

    @GetMapping("/deletestudent/{id}")
    public String deleteStudent(@PathVariable("id") int id, Model model) {
        Student student = studentRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
        studentRepository.delete(student);

        return "redirect:/students";
    }

}
