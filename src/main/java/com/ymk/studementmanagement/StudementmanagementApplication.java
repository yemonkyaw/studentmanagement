package com.ymk.studementmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
public class StudementmanagementApplication {

	public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Yangon"));
		SpringApplication.run(StudementmanagementApplication.class, args);
	}

	@PostConstruct
	void setDefaultTimezone()  {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
	}

}
