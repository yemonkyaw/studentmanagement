package com.ymk.studementmanagement.service;

import com.ymk.studementmanagement.entity.Attendance;
import com.ymk.studementmanagement.entity.Classes;
import com.ymk.studementmanagement.entity.paging.Paged;
import com.ymk.studementmanagement.entity.paging.Paging;
import com.ymk.studementmanagement.repository.AttendanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttendanceService {

    @Autowired
    private AttendanceRepository attendanceRepository;

    public void addAttendance(Attendance attendance)
    {
        attendanceRepository.save(attendance);
    }

    public Paged<Attendance> getAllAttendancesWithPaged(int pageNumber, int size){
        PageRequest pageRequest = PageRequest.of(pageNumber-1, size, Sort.by(Sort.Direction.DESC, "aid"));
        Page<Attendance> attendances = attendanceRepository.findAll(pageRequest);
        return new Paged<>(attendances, Paging.of(attendances.getTotalPages(), pageNumber, size));
    }

    public List<Attendance> getAllAttendances(){
        return attendanceRepository.findAll();
    }

    public Attendance saveUpdate(Attendance attendance){
        return attendanceRepository.save(attendance);
    }

    public Attendance findById(int id){
        return attendanceRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid attendance Id:" + id));
    }

    public void delete(Attendance attendance){
        attendanceRepository.delete(attendance);
    }
}
