package com.ymk.studementmanagement.service;

import com.ymk.studementmanagement.entity.Attendance;
import com.ymk.studementmanagement.entity.Exam;
import com.ymk.studementmanagement.entity.Mark;
import com.ymk.studementmanagement.entity.paging.Paged;
import com.ymk.studementmanagement.entity.paging.Paging;
import com.ymk.studementmanagement.repository.MarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class MarkService {

    @Autowired
    private MarkRepository markRepository;

    public Paged<Mark> getAllMarksWithPaged(int pageNumber, int size){
        PageRequest pageRequest = PageRequest.of(pageNumber-1, size, Sort.by(Sort.Direction.DESC, "mid"));
        Page<Mark> marks = markRepository.findAll(pageRequest);
        return new Paged<>(marks, Paging.of(marks.getTotalPages(), pageNumber, size));
    }

    public Mark saveUpdate(Mark mark){
        return markRepository.save(mark);
    }

    public Mark findById(int id){
        return markRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid exam Id:" + id));
    }

    public void delete(Mark mark){
        markRepository.delete(mark);
    }

}
