package com.ymk.studementmanagement.service;

import com.ymk.studementmanagement.entity.Attendance;
import com.ymk.studementmanagement.entity.Exam;
import com.ymk.studementmanagement.entity.paging.Paged;
import com.ymk.studementmanagement.entity.paging.Paging;
import com.ymk.studementmanagement.repository.ExamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamService {

    @Autowired
    private ExamRepository examRepository;

    public Paged<Exam> getAllExamsWithPaged(int pageNumber, int size){
        PageRequest pageRequest = PageRequest.of(pageNumber-1, size, Sort.by(Sort.Direction.DESC, "eid"));
        Page<Exam> exams = examRepository.findAll(pageRequest);
        return new Paged<>(exams, Paging.of(exams.getTotalPages(), pageNumber, size));
    }

    public Exam saveUpdate(Exam exam){
        return examRepository.save(exam);
    }

    public Exam findById(int id){
        return examRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid exam Id:" + id));
    }

    public void delete(Exam exam){
        examRepository.delete(exam);
    }

    public List<Exam> getAllExams(){
        return examRepository.findAll();
    }

}
