package com.ymk.studementmanagement.service;

import com.ymk.studementmanagement.entity.Event;
import com.ymk.studementmanagement.entity.paging.Paged;
import com.ymk.studementmanagement.entity.paging.Paging;
import com.ymk.studementmanagement.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    public Paged<Event> getAllEventsWithPaged(int pageNumber, int size){
        PageRequest pageRequest = PageRequest.of(pageNumber-1, size, Sort.by(Sort.Direction.DESC, "id"));
        Page<Event> events = eventRepository.findAll(pageRequest);
        return new Paged<>(events, Paging.of(events.getTotalPages(), pageNumber, size));
    }

    public Event saveUpdate(Event event){
        System.out.println("Save Event");
        return eventRepository.save(event);
    }

    public Event findById(int id){
        return eventRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid Event Id:" + id));
    }

    public void delete(Event event){
        eventRepository.delete(event);
    }

}
