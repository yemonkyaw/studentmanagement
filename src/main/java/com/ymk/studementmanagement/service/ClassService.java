package com.ymk.studementmanagement.service;

import com.ymk.studementmanagement.entity.Classes;
import com.ymk.studementmanagement.entity.paging.Paged;
import com.ymk.studementmanagement.entity.paging.Paging;
import com.ymk.studementmanagement.repository.ClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassService {
    @Autowired
    private ClassRepository classRepository;

    public Classes saveUpdate(Classes classes){
        return classRepository.save(classes);
    }

    public Paged<Classes> getClassesWithPaged(int pageNumber, int size){
        PageRequest pageRequest = PageRequest.of(pageNumber-1, size);
        Page<Classes> classesPage = classRepository.findAll(pageRequest);
        return new Paged<Classes>(classesPage, Paging.of(classesPage.getTotalPages(), pageNumber, size));
    }

    public Classes findById(int id){
        return classRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid classes Id:" + id));
    }

    public void delete(Classes classes){
        classRepository.delete(classes);
    }

    public List<Classes> getAllClasses(){
        return classRepository.findAll();
    }
}
