package com.ymk.studementmanagement.service;
import com.ymk.studementmanagement.entity.Classes;
import com.ymk.studementmanagement.entity.Student;
import com.ymk.studementmanagement.entity.paging.Paged;
import com.ymk.studementmanagement.entity.paging.Paging;
import com.ymk.studementmanagement.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public void addStudent(Student student)
    {
        studentRepository.save(student);
    }

    public Paged<Student> getAllStudentsWithPaged(int pageNumber, int size){
        PageRequest pageRequest = PageRequest.of(pageNumber-1, size, Sort.by(Sort.Direction.DESC, "sid"));
        Page<Student> students = studentRepository.findAll(pageRequest);
        return new Paged<>(students, Paging.of(students.getTotalPages(), pageNumber, size));
    }

    public List<Student> getAllStudents(){
        return studentRepository.findAll();
    }

}
