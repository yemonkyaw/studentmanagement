package com.ymk.studementmanagement.service;

import com.ymk.studementmanagement.entity.Attendance;
import com.ymk.studementmanagement.entity.Subject;
import com.ymk.studementmanagement.entity.paging.Paged;
import com.ymk.studementmanagement.entity.paging.Paging;
import com.ymk.studementmanagement.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectService {
    @Autowired
    private SubjectRepository subjectRepository;

    public Subject saveUpdate(Subject subject){
        return subjectRepository.save(subject);
    }

    public Paged<Subject> getSubjectsWithPaged(int pageNumber, int size){
        PageRequest pageRequest = PageRequest.of(pageNumber-1, size);
        Page<Subject> subjectPage = subjectRepository.findAll(pageRequest);
        return new Paged<Subject>(subjectPage, Paging.of(subjectPage.getTotalPages(), pageNumber, size));
    }

    public Subject findById(int id){
        return subjectRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid subject Id:" + id));
    }

    public void delete(Subject subject){
        subjectRepository.delete(subject);
    }

    public List<Subject> getAllSubjects(){
        return subjectRepository.findAll();
    }
}
