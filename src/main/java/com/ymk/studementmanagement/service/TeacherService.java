package com.ymk.studementmanagement.service;

import com.ymk.studementmanagement.entity.Student;
import com.ymk.studementmanagement.entity.Teacher;
import com.ymk.studementmanagement.entity.paging.Paged;
import com.ymk.studementmanagement.entity.paging.Paging;
import com.ymk.studementmanagement.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {
    @Autowired
    private TeacherRepository teacherRepository;

    public Teacher createUpdateTeacher(Teacher teacher){
        return teacherRepository.save(teacher);
    }

    public Paged<Teacher> getTeacherPaginaion(int pageNumber, int size){
        PageRequest pageRequest = PageRequest.of(pageNumber-1, size);
        Page<Teacher> teacherPage = teacherRepository.findAll(pageRequest);
        return new Paged<Teacher>(teacherPage, Paging.of(teacherPage.getTotalPages(), pageNumber, size));
    }

    public Teacher findById(int id){
        return teacherRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid teacher Id:" + id));
    }

    public void deleteTeacher(Teacher teacher){
        teacherRepository.delete(teacher);
    }

    public List<Teacher> getAllTeacher(){
        return teacherRepository.findAll();
    }
}
