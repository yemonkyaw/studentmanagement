-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: SM
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sm_attendance`
--

DROP TABLE IF EXISTS `sm_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_attendance` (
  `aid` int unsigned NOT NULL AUTO_INCREMENT,
  `class_id` varchar(15) DEFAULT NULL,
  `absents` text,
  `date` date DEFAULT NULL,
  `entry` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_attendance`
--

LOCK TABLES `sm_attendance` WRITE;
/*!40000 ALTER TABLE `sm_attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_class`
--

DROP TABLE IF EXISTS `sm_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_class` (
  `cid` int NOT NULL AUTO_INCREMENT,
  `c_numb` varchar(128) DEFAULT NULL,
  `c_name` varchar(128) DEFAULT NULL,
  `teacher_id` int DEFAULT NULL,
  `c_capacity` int DEFAULT NULL,
  `c_loc` varchar(60) DEFAULT NULL,
  `c_sdate` date DEFAULT NULL,
  `c_edate` date DEFAULT NULL,
  PRIMARY KEY (`cid`),
  KEY `fk_sm_class_1_idx` (`teacher_id`),
  CONSTRAINT `fk_sm_class_1` FOREIGN KEY (`teacher_id`) REFERENCES `sm_teacher` (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_class`
--

LOCK TABLES `sm_class` WRITE;
/*!40000 ALTER TABLE `sm_class` DISABLE KEYS */;
INSERT INTO `sm_class` VALUES (3,'5','test class',2,110,NULL,'2021-08-17','2022-04-25');
/*!40000 ALTER TABLE `sm_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_events`
--

DROP TABLE IF EXISTS `sm_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_events` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `start` varchar(50) DEFAULT NULL,
  `end` varchar(50) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `title` text,
  `description` longtext,
  `color` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_events`
--

LOCK TABLES `sm_events` WRITE;
/*!40000 ALTER TABLE `sm_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_exam`
--

DROP TABLE IF EXISTS `sm_exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_exam` (
  `eid` int NOT NULL AUTO_INCREMENT,
  `classid` int DEFAULT NULL,
  `subject_id` varchar(128) DEFAULT NULL,
  `e_name` varchar(128) DEFAULT NULL,
  `e_s_date` date DEFAULT NULL,
  `e_e_date` date DEFAULT NULL,
  `entry_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_exam`
--

LOCK TABLES `sm_exam` WRITE;
/*!40000 ALTER TABLE `sm_exam` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_exam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_fee_payment_history`
--

DROP TABLE IF EXISTS `sm_fee_payment_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_fee_payment_history` (
  `payment_history_id` bigint NOT NULL AUTO_INCREMENT,
  `fees_pay_id` int NOT NULL,
  `amount` float NOT NULL,
  `payment_method` varchar(50) NOT NULL,
  `paid_date` date NOT NULL,
  `paid_by` bigint NOT NULL,
  `paid_status` int NOT NULL,
  `paymentdescription` text NOT NULL,
  PRIMARY KEY (`payment_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_fee_payment_history`
--

LOCK TABLES `sm_fee_payment_history` WRITE;
/*!40000 ALTER TABLE `sm_fee_payment_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_fee_payment_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_fees`
--

DROP TABLE IF EXISTS `sm_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_fees` (
  `fees_id` int NOT NULL AUTO_INCREMENT,
  `class_id` int NOT NULL,
  `order_id` int NOT NULL,
  `student_id` int NOT NULL,
  `fees_amount` float NOT NULL,
  `description` text NOT NULL,
  `duration` text NOT NULL,
  `paymentType` text NOT NULL,
  `due_time` int NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int NOT NULL,
  PRIMARY KEY (`fees_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_fees`
--

LOCK TABLES `sm_fees` WRITE;
/*!40000 ALTER TABLE `sm_fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_fees_payment`
--

DROP TABLE IF EXISTS `sm_fees_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_fees_payment` (
  `fees_pay_id` int NOT NULL AUTO_INCREMENT,
  `class_id` int NOT NULL,
  `student_id` bigint NOT NULL,
  `fees_id` int NOT NULL,
  `fees_paid_amount` float NOT NULL,
  `payment_status` varchar(10) NOT NULL,
  `paid_due_date` date NOT NULL,
  PRIMARY KEY (`fees_pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_fees_payment`
--

LOCK TABLES `sm_fees_payment` WRITE;
/*!40000 ALTER TABLE `sm_fees_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_fees_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_grade`
--

DROP TABLE IF EXISTS `sm_grade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_grade` (
  `gid` int NOT NULL AUTO_INCREMENT,
  `g_name` varchar(60) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `g_point` varchar(5) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `mark_from` int DEFAULT NULL,
  `mark_upto` int DEFAULT NULL,
  `comment` varchar(60) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_grade`
--

LOCK TABLES `sm_grade` WRITE;
/*!40000 ALTER TABLE `sm_grade` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_grade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_import_history`
--

DROP TABLE IF EXISTS `sm_import_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_import_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` int NOT NULL,
  `imported_id` longtext NOT NULL,
  `time` datetime NOT NULL,
  `count` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_import_history`
--

LOCK TABLES `sm_import_history` WRITE;
/*!40000 ALTER TABLE `sm_import_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_import_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_leavedays`
--

DROP TABLE IF EXISTS `sm_leavedays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_leavedays` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `class_id` int NOT NULL,
  `leave_date` date DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_leavedays`
--

LOCK TABLES `sm_leavedays` WRITE;
/*!40000 ALTER TABLE `sm_leavedays` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_leavedays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_mark`
--

DROP TABLE IF EXISTS `sm_mark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_mark` (
  `mid` bigint NOT NULL AUTO_INCREMENT,
  `subject_id` varchar(128) DEFAULT NULL,
  `class_id` int DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `exam_id` int DEFAULT NULL,
  `mark` varchar(60) DEFAULT NULL,
  `remarks` text,
  `attendance` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_mark`
--

LOCK TABLES `sm_mark` WRITE;
/*!40000 ALTER TABLE `sm_mark` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_mark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_mark_extract`
--

DROP TABLE IF EXISTS `sm_mark_extract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_mark_extract` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `student_id` bigint DEFAULT NULL,
  `field_id` bigint DEFAULT NULL,
  `exam_id` int DEFAULT NULL,
  `subject_id` int DEFAULT NULL,
  `mark` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_mark_extract`
--

LOCK TABLES `sm_mark_extract` WRITE;
/*!40000 ALTER TABLE `sm_mark_extract` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_mark_extract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_mark_fields`
--

DROP TABLE IF EXISTS `sm_mark_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_mark_fields` (
  `field_id` bigint NOT NULL AUTO_INCREMENT,
  `subject_id` int DEFAULT NULL,
  `field_text` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_mark_fields`
--

LOCK TABLES `sm_mark_fields` WRITE;
/*!40000 ALTER TABLE `sm_mark_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_mark_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_messages`
--

DROP TABLE IF EXISTS `sm_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_messages` (
  `mid` int NOT NULL AUTO_INCREMENT,
  `s_id` int DEFAULT NULL,
  `r_id` int DEFAULT NULL,
  `subject` varchar(250) DEFAULT NULL,
  `msg` longtext,
  `replay_id` int DEFAULT NULL,
  `main_m_id` int DEFAULT NULL,
  `del_stat` int DEFAULT NULL,
  `s_read` int DEFAULT '0',
  `r_read` int DEFAULT '0',
  `m_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_messages`
--

LOCK TABLES `sm_messages` WRITE;
/*!40000 ALTER TABLE `sm_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_messages_delete`
--

DROP TABLE IF EXISTS `sm_messages_delete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_messages_delete` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `m_id` bigint NOT NULL,
  `user_id` bigint NOT NULL,
  `delete_status` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_messages_delete`
--

LOCK TABLES `sm_messages_delete` WRITE;
/*!40000 ALTER TABLE `sm_messages_delete` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_messages_delete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_notification`
--

DROP TABLE IF EXISTS `sm_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_notification` (
  `nid` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `type` int DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`nid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_notification`
--

LOCK TABLES `sm_notification` WRITE;
/*!40000 ALTER TABLE `sm_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_settings`
--

DROP TABLE IF EXISTS `sm_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(50) DEFAULT NULL,
  `option_value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_settings`
--

LOCK TABLES `sm_settings` WRITE;
/*!40000 ALTER TABLE `sm_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_student`
--

DROP TABLE IF EXISTS `sm_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_student` (
  `sid` int NOT NULL AUTO_INCREMENT,
  `s_rollno` varchar(15) DEFAULT NULL,
  `s_name` varchar(30) DEFAULT NULL,
  `s_dob` date DEFAULT NULL,
  `s_gender` varchar(10) DEFAULT NULL,
  `s_paddress` varchar(200) DEFAULT NULL,
  `s_phone` varchar(25) DEFAULT NULL,
  `s_bloodgrp` varchar(20) DEFAULT NULL,
  `s_doj` date DEFAULT NULL,
  `class_id` varchar(255) DEFAULT NULL,
  `class_date` varchar(255) DEFAULT NULL,
  `p_name` varchar(30) DEFAULT NULL,
  `p_gender` varchar(10) DEFAULT NULL,
  `p_edu` varchar(50) DEFAULT NULL,
  `p_phone` varchar(25) DEFAULT NULL,
  `p_profession` varchar(60) DEFAULT NULL,
  `p_bloodgrp` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_student`
--

LOCK TABLES `sm_student` WRITE;
/*!40000 ALTER TABLE `sm_student` DISABLE KEYS */;
INSERT INTO `sm_student` VALUES (1,'23','Mg Mg','1995-08-16','Male','Monywa',NULL,'A+','2021-08-06','A','Tomorrow','KSW','Male','No','09887877878','Taung Thu','B-');
/*!40000 ALTER TABLE `sm_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_subject`
--

DROP TABLE IF EXISTS `sm_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_subject` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sub_code` varchar(8) DEFAULT NULL,
  `class_id` int DEFAULT NULL,
  `sub_name` varchar(60) DEFAULT NULL,
  `sub_teach_id` int DEFAULT NULL,
  `book_name` varchar(60) DEFAULT NULL,
  `sub_desc` varchar(250) DEFAULT NULL,
  `max_mark` int DEFAULT NULL,
  `pass_mark` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sm_subject_class_idx` (`class_id`),
  KEY `fk_sm_subject_teacher_idx` (`sub_teach_id`),
  CONSTRAINT `fk_sm_subject_class` FOREIGN KEY (`class_id`) REFERENCES `sm_class` (`cid`),
  CONSTRAINT `fk_sm_subject_teacher` FOREIGN KEY (`sub_teach_id`) REFERENCES `sm_teacher` (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_subject`
--

LOCK TABLES `sm_subject` WRITE;
/*!40000 ALTER TABLE `sm_subject` DISABLE KEYS */;
INSERT INTO `sm_subject` VALUES (2,'Eng',3,'English',2,'Reading English','',100,40);
/*!40000 ALTER TABLE `sm_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_teacher`
--

DROP TABLE IF EXISTS `sm_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_teacher` (
  `tid` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `doj` date DEFAULT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `qualification` varchar(25) DEFAULT NULL,
  `gender` varchar(12) DEFAULT NULL,
  `bloodgrp` varchar(5) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `whours` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_teacher`
--

LOCK TABLES `sm_teacher` WRITE;
/*!40000 ALTER TABLE `sm_teacher` DISABLE KEYS */;
INSERT INTO `sm_teacher` VALUES (2,'fsda','fdsa','2021-08-11','2021-08-26','09877783423','Bsc','Male','A+','Teacher','5 Hrs');
/*!40000 ALTER TABLE `sm_teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_teacher_attendance`
--

DROP TABLE IF EXISTS `sm_teacher_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_teacher_attendance` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `teacher_id` bigint NOT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leave_date` date DEFAULT NULL,
  `reason` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_teacher_attendance`
--

LOCK TABLES `sm_teacher_attendance` WRITE;
/*!40000 ALTER TABLE `sm_teacher_attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_teacher_attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_temp`
--

DROP TABLE IF EXISTS `sm_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_temp` (
  `t_id` int NOT NULL AUTO_INCREMENT,
  `t_name` varchar(255) DEFAULT NULL,
  `t_username` varchar(255) DEFAULT NULL,
  `t_email` varchar(255) DEFAULT NULL,
  `t_password` varchar(255) DEFAULT NULL,
  `t_type` varchar(255) NOT NULL,
  `t_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `t_active` int DEFAULT '1',
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_temp`
--

LOCK TABLES `sm_temp` WRITE;
/*!40000 ALTER TABLE `sm_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_timetable`
--

DROP TABLE IF EXISTS `sm_timetable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_timetable` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int NOT NULL,
  `time_id` int NOT NULL,
  `subject_id` int NOT NULL,
  `session_id` int NOT NULL,
  `day` int NOT NULL,
  `heading` text,
  `is_active` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_timetable`
--

LOCK TABLES `sm_timetable` WRITE;
/*!40000 ALTER TABLE `sm_timetable` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_timetable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_transport`
--

DROP TABLE IF EXISTS `sm_transport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_transport` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `bus_no` varchar(30) DEFAULT NULL,
  `bus_name` varchar(50) DEFAULT NULL,
  `driver_name` varchar(50) DEFAULT NULL,
  `bus_route` mediumtext,
  `route_fees` varchar(5) DEFAULT NULL,
  `phone_no` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_transport`
--

LOCK TABLES `sm_transport` WRITE;
/*!40000 ALTER TABLE `sm_transport` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_transport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_users`
--

DROP TABLE IF EXISTS `sm_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_users` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `password` varchar(108) NOT NULL,
  `enable` tinyint DEFAULT '1',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid_UNIQUE` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_users`
--

LOCK TABLES `sm_users` WRITE;
/*!40000 ALTER TABLE `sm_users` DISABLE KEYS */;
INSERT INTO `sm_users` VALUES (1,'ymk','$2a$10$gGzy7vY5DEI90s4n3D6coOsGvJ.f7r032tuI7PPTcqwVnbne3J9Nm',1);
/*!40000 ALTER TABLE `sm_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_workinghours`
--

DROP TABLE IF EXISTS `sm_workinghours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sm_workinghours` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `hour` varchar(20) DEFAULT NULL,
  `begintime` varchar(10) NOT NULL,
  `endtime` varchar(10) NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_workinghours`
--

LOCK TABLES `sm_workinghours` WRITE;
/*!40000 ALTER TABLE `sm_workinghours` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_workinghours` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-21 17:49:47
